CREATE SCHEMA bank;

CREATE TABLE bank.account (
    id bigint NOT NULL PRIMARY KEY,
    account_number CHAR(8) NOT NULL,
    current_balance NUMERIC(10,3) NOT NULL,
    bank_name VARCHAR(50) NOT NULL,
    owner_name VARCHAR(50) NOT NULL,
    UNIQUE (account_number)
);

CREATE SEQUENCE bank.transaction_sequence START WITH 5;
CREATE TABLE bank.transaction (
    id bigint NOT NULL PRIMARY KEY,
    source_account_id bigint NOT NULL REFERENCES bank.account(id),
    target_account_id bigint NOT NULL REFERENCES bank.account(id),
    source_owner_name varchar(50) NOT NULL,
    target_owner_name varchar(50) NOT NULL,
    amount NUMERIC(10,3) NOT NULL,
    initiation_date timestamp NOT NULL,
    completion_date TIMESTAMP,
    reference VARCHAR(255)
);
