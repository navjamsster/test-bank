package com.test.bank.services;


import com.test.bank.models.Account;
import com.test.bank.models.Transaction;
import com.test.bank.models.TransferInput;
import com.test.bank.repositories.AccountRepository;
import com.test.bank.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TransactionService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public boolean makeTransfer(TransferInput transferInput) {
        Long sourceAccountNumber = transferInput.getSourceAccount().getAccountNumber();
        Optional<Account> sourceAccount = accountRepository
                .findByAccountNumber(sourceAccountNumber);

        Long targetAccountNumber = transferInput.getTargetAccount().getAccountNumber();
        Optional<Account> targetAccount = accountRepository
                .findByAccountNumber(targetAccountNumber);

        if (sourceAccount.isPresent() && targetAccount.isPresent()) {
            if (isAmountAvailable(transferInput.getAmount(), sourceAccount.get().getCurrentBalance())) {

                Transaction transaction = new Transaction();
                transaction.setAmount(transferInput.getAmount());
                transaction.setSourceAccountId(sourceAccount.get().getAccountNumber());
                transaction.setSourceOwnerName(targetAccount.get().getOwnerName());
                transaction.setTargetAccountId(targetAccount.get().getAccountNumber());
                transaction.setTargetOwnerName(targetAccount.get().getOwnerName());
                transaction.setInitiationDate(LocalDateTime.now());
                transaction.setCompletionDate(LocalDateTime.now());
                transaction.setReference(transferInput.getReference());

                updateAccountBalanceSource(sourceAccount.get(), transferInput.getAmount());
                updateAccountBalanceTarget(targetAccount.get(), transferInput.getAmount());

                transactionRepository.save(transaction);

                return true;
            }
        }
        return false;
    }

    private void updateAccountBalanceSource(Account account, double amount) {
        account.setCurrentBalance((account.getCurrentBalance() - amount));
        accountRepository.save(account);
    }

    private void updateAccountBalanceTarget(Account account, double amount) {
        account.setCurrentBalance((account.getCurrentBalance() + amount));
        accountRepository.save(account);
    }

    private boolean isAmountAvailable(double amount, double accountBalance) {
        return (accountBalance - amount) > 0;
    }
}
