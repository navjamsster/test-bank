package com.test.bank.services;


import com.test.bank.models.Account;
import com.test.bank.repositories.AccountRepository;
import com.test.bank.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public Account getAccount(Long accountNumber) {
        Optional<Account> account = accountRepository.findByAccountNumber(accountNumber);

        account.ifPresent(value ->
                value.setTransactions(transactionRepository
                        .findBySourceAccountIdOrderByInitiationDate(accountNumber)));

        return account.orElse(null);
    }
}