package com.test.bank.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TransferInput {

    private AccountInput sourceAccount;
    private AccountInput targetAccount;
    @Positive(message = "Transfer amount must be positive")
    @Min(value = 1, message = "Amount must be larger than 1")
    private double amount;
    private String reference;

    public AccountInput getSourceAccount() {
        return sourceAccount;
    }

    public AccountInput getTargetAccount() {
        return targetAccount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransactionInput{" +
                "sourceAccount=" + sourceAccount +
                ", targetAccount=" + targetAccount +
                ", amount=" + amount +
                ", reference='" + reference + '\'' +
                '}';
    }

}
