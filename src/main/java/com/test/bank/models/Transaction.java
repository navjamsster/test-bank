package com.test.bank.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transaction", schema = "bank")
@Data
@SequenceGenerator(name = "transaction_seq", sequenceName = "transaction_sequence", schema = "bank", initialValue = 5)
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_seq")
    private long id;
    private long sourceAccountId;
    private String sourceOwnerName;
    private long targetAccountId;
    private String targetOwnerName;
    private double amount;
    private LocalDateTime initiationDate;
    private LocalDateTime completionDate;
    private String reference;
}
