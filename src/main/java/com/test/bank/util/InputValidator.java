package com.test.bank.util;

import com.test.bank.models.AccountInput;
import com.test.bank.models.TransferInput;

import java.util.regex.Pattern;

public class InputValidator {
    private static final Pattern accountNumberPattern = Pattern.compile("^[0-9]{8}$");

    public static boolean isSearchTransactionValid(TransferInput transferInput) {
        if (!isSearchCriteriaValid(transferInput.getSourceAccount()))
            return false;

        if (!isSearchCriteriaValid(transferInput.getTargetAccount()))
            return false;

        return !transferInput.getSourceAccount().equals(transferInput.getTargetAccount());
    }

    public static boolean isSearchCriteriaValid(AccountInput accountInput) {
        return accountNumberPattern.matcher(accountInput.getAccountNumber().toString()).find();
    }

}
