package com.test.bank.util;

public class IConstants {
    public static final String INVALID_SEARCH_CRITERIA = "account number is not valid";
    public static final String NO_ACCOUNT_FOUND = "Unable to find an account related to account number";
    public static final String INVALID_TRANSACTION = "Account information is invalid or transaction has been denied for your protection. Please try again.";
}
